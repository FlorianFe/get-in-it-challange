
const chalk = require('chalk');

const graph = require('./data/generatedGraph.json');
const findShortestPath = require('./src/findShortestPath/findShortestPath');

{
    const green = chalk.hex('#76ff03');
    const blue = chalk.hex('#00b0ff');

    try
    {
        const { path : shortestPath, cost: shortestPathCost } = findShortestPath(graph, 'Erde', 'b3-r7-r4nd7');
        
        console.log('Die Route führt der Reihe nach über folgende Planeten: \n\n' + shortestPath.map((station, index) => `${index + 1}. ` + green(station) + '\n').join(''));
        console.log('Die Strecke hat eine Gesamtlänge von: \n' + blue(shortestPathCost) + '\n');
    }
    catch(error)
    {
        console.error('Die Route konnte nicht berechnet werden!');
        console.error(error);
    }
}

