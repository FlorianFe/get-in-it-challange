
const { FibonacciHeap } = require('@tyriar/fibonacci-heap');
const generateAdjacencyQueues = require('./generateAdjacencyQueues/generateAdjacencyQueues');

// Runtime Cost: O(|V| * log(|V|) + |E|)
const findShortestPath = (graph, sourceName, targetName) => 
{
    const numberOfGraphNodes = graph.nodes.length;
    const source = graph.nodes.findIndex((node) => node.label === sourceName);
    const target = graph.nodes.findIndex((node) => node.label === targetName);

    if(source === undefined) throw Error(`Can't find node with name ${sourceName}!`);
    if(target === undefined) throw Error(`Can't find node with name ${targetName}!`);
    
    let dijkstraQueue = new FibonacciHeap();
    let adjacencyQueues = generateAdjacencyQueues(graph);
    let nodesMarked = new Array(numberOfGraphNodes).fill(false);
    let distancesToNodes = new Array(numberOfGraphNodes).fill(Infinity);

    dijkstraQueue.insert(0.0, { target: source, path: [] });

    while(!dijkstraQueue.isEmpty())
    {
        const { key : totalDistance, value : transition } = dijkstraQueue.extractMinimum();

        if(transition.target === target)
        {
            const fullPath = [...transition.path, transition.target];

            return { 
                path: fullPath.map((station) => graph.nodes[station].label), 
                cost: totalDistance 
            };
        }

        let neighbourAdjacencyQueue = adjacencyQueues[transition.target];

        while(!neighbourAdjacencyQueue.isEmpty())
        {
            const { key: nearestNeighbourDistance, value: nearestNeighbourIndex } = neighbourAdjacencyQueue.extractMinimum();
            
            if(!nodesMarked[nearestNeighbourIndex])
            {
                const nextTotalDistance = totalDistance + nearestNeighbourDistance;
                
                distancesToNodes[nearestNeighbourIndex] = nextTotalDistance;
                
                dijkstraQueue.insert(nextTotalDistance, 
                    { 
                        target: nearestNeighbourIndex, 
                        path: [...transition.path, transition.target]
                    }
                );
            }
        }

        nodesMarked[transition.target] = true;
    }

    throw Error(`There is no path from node ${graph.nodes[source].label} to node ${graph.nodes[target].label}!`);
};

module.exports = findShortestPath;