
const { FibonacciHeap } = require('@tyriar/fibonacci-heap');

// Runtime Cost: O(|V| + |E|)
const generateAdjacencyQueues = (graph) => 
{
    let adjacencyQueues = graph.nodes.map(() => new FibonacciHeap());

    graph.edges.forEach((edge) => 
    {
        const { source, target, cost } = edge;

        adjacencyQueues[source].insert(cost, target);
        adjacencyQueues[target].insert(cost, source);
    });

    return adjacencyQueues;
};

module.exports = generateAdjacencyQueues;