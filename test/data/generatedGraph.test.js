
const test = require('ava');
const JSONValidator = require('jsonschema').Validator;

const isObjectMatchingSchema = (instance, schema) => 
{
    const validator = new JSONValidator(); 
    const validation = validator.validate(instance, schema);
    const validationErrors = validation.errors;
    
    return validationErrors.length === 0;
};  


test('0.0 graph structure has expected structure', (t) => 
{
    let graph = require('./../../data/generatedGraph.json');
    const schema = require('./expectedSchema.json');
    
    t.true(isObjectMatchingSchema(graph, schema));
});

test('1.0 the edge costs are not a metric by violating the triangle inequalation', (t) => 
{
    let graph = require('./../../data/generatedGraph.json');
    let edges = graph.edges;

    let ways = new Array(graph.nodes.length).fill(0).map(
        () => new Array(graph.nodes.length).fill(Infinity)
    );

    edges.forEach((edge) => 
    {
        ways[edge.source][edge.target] = edge.cost;
        ways[edge.target][edge.source] = edge.cost;
    });

    let pass = true;

    for(let i=0; i<ways.length; i++)
    {
        for(let j=i; j<ways.length; j++)
        {
            for(let k=j; k<ways.length; k++)
            {
                if(i !== Infinity && j !== Infinity && k !== Infinity)
                {
                    if(ways[i][k] <= ways[i][j] + ways[j][k])
                    {
                        pass = false;
                    }
                }
            }
        }
    }
       

    t.false(pass);
});


test('2.0 all edges have a non-negative weight', (t) => 
{
    let graph = require('./../../data/generatedGraph.json');

    t.true(graph.edges.every((edge) => edge.cost >= 0));
});


test('2.1 all edges have a weight between 0.0 and 1.0', (t) => 
{
    let graph = require('./../../data/generatedGraph.json');

    t.true(graph.edges.every((edge) => edge.cost >= 0.0 && edge.cost <= 1.0));
});


test('3.0 the graph has more than one component', (t) => 
{
    let graph = require('./../../data/generatedGraph.json');
    
    const sourceLabel = 'Erde';
    const source = graph.nodes.findIndex((node) => node.label === sourceLabel);

    let marks = new Array(graph.nodes.length).fill(-1);

    let counter = 0;

    graph.edges.forEach((edge) => 
    {
        if(marks[edge.source] === -1 && marks[edge.target] === -1)
        {
            marks[edge.source] = counter;
            marks[edge.target] = counter;

            counter++;
        }
        else
        {
            if(marks[edge.source] !== -1 && marks[edge.target] !== -1)
            {
                const a = marks[edge.source];
                const b = marks[edge.target];

                marks = marks.map((mark) => (mark === a || mark === b) ? b : mark);
            }
            else
            {
                const value = Math.max(marks[edge.source], marks[edge.target]);
            
                marks[edge.source] = value;
                marks[edge.target] = value;
            }
        }       
    });

    t.log('Nodes not connected with earth: ' + marks.filter((mark) => mark !== marks[source]).length);

    t.false(marks.every((mark) => mark === marks[0]));
});

test('3.1 there are isolated nodes', (t) => 
{
    let graph = require('./../../data/generatedGraph.json');

    let marks = new Array(graph.nodes.length).fill(-1);

    graph.edges.forEach((edge) => 
    {
        marks[edge.source] = 1;
        marks[edge.target] = 1;
    });

    t.log('Isolated nodes: ' + marks.filter((mark) => mark !== 1).length);

    t.false(marks.every((mark) => mark === 1));
});