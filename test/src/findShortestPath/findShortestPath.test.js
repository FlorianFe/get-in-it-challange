
const test = require('ava');

const findShortestPath = require('../../../src/findShortestPath/findShortestPath');


test('unconnected graph will cause in Error', (t) => 
{
    let graph = { nodes: [ { label: 'a' }, { label: 'b' } ], edges: [] };

    try
    {
        findShortestPath(graph, 'a', 'b');
        t.fail();
    }
    catch(error)
    {
        t.log(error.message);
        t.pass();
    }
});

test('fully connected graph won\'t cause an error', (t) => 
{
    let graph = { nodes: [ { label: 'a' }, { label: 'b' } ], edges: [ { source: 0, target: 1, cost: 1.0 } ] };

    try
    {
        const { path, cost } = findShortestPath(graph, 'a', 'b');
        t.log(path.join(' -> ') + ' cost: ' + cost);
        t.pass();
    }
    catch(error)
    {
        t.log(error.message);
        t.fail();
    }
});

test('costs are calculated correctly', (t) => 
{
    let graph = { 
        nodes: 
        [ 
            { label: 'a' }, 
            { label: 'b' }, 
            { label: 'c' }, 
            { label: 'd' } 
        ], 
        edges: 
        [ 
            { source: 0, target: 1, cost: 1.5 },
            { source: 1, target: 2, cost: 0.0 },
            { source: 2, target: 3, cost: 3.5 }, 
        ] 
    };

    try
    {
        const { cost } = findShortestPath(graph, 'a', 'd');
        t.true(cost === 5.0);
    }
    catch(error)
    {
        t.log(error.message);
        t.fail();
    }
});

test('path is calculated correctly', (t) => 
{
    let graph = { 
        nodes: 
        [ 
            { label: 'a' }, 
            { label: 'b' }, 
            { label: 'c' }, 
            { label: 'd' } 
        ], 
        edges: 
        [ 
            { source: 0, target: 1, cost: 1.5 },
            { source: 1, target: 2, cost: 0.0 },
            { source: 2, target: 3, cost: 3.5 }, 
        ] 
    };

    try
    {
        const { path } = findShortestPath(graph, 'a', 'd');

        t.log(path.join(' -> '));
        
        t.true(path[0] === 'a');
        t.true(path[1] === 'b');
        t.true(path[2] === 'c');
        t.true(path[3] === 'd');
    }
    catch(error)
    {
        t.log(error.message);
        t.fail();
    }
});